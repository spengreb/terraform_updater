#!/usr/bin/env bash

TERRA_VER=0.12.3
INSTALL_DIR=/usr/bin

read -p "Desired Terraform Version: " TERRA_VER

wget -P /tmp/ https://releases.hashicorp.com/terraform/${TERRA_VER}/terraform_${TERRA_VER}_linux_amd64.zip 

unzip /tmp/terraform_${TERRA_VER}_linux_amd64.zip -d /tmp/

mv /tmp/terraform $(which terraform)
